import json


def granted() -> None:
    try:
        granted_read = open("db/granted.txt", "r")
        granted_read.close()
    except:
        create = open("db/granted.txt", "w")  # создание файла
        create.close()


def granted_save(id: str) -> None:
    granted_read = open("db/granted.txt", "r")
    mass = granted_read.read().splitlines()
    mass.append(id)
    granted_read.close()
    granted_write = open("db/granted.txt", "w")
    for e in mass:
        granted_write.write(e + "\n")
    granted_write.close()


def granted_get() -> list:
    granted_read = open("db/key", "r")
    return granted_read.read().splitlines()


def key_save(key: str) -> None:
    granted_write = open("db/key", "w")
    granted_write.write(key)
    granted_write.close()


def key_get() -> str:
    granted_read = open("db/key", "r")
    return granted_read.read()


def ini() -> dict or None:
    try:
        db_read = open("db/db.json", "r")
    except:
        print("creating a db file")
        create = open("db/db.json", "w")  # создание файла
        create.close()
        db_read = open("db/db.json", "r")
    if not db_read.read(1):  # заполнение дб если пустая
        print("db is empty, creating a dictionary")
        db_read.close()
        db_write = open("db/db.json", "w")
        json.dump({}, db_write)
        db_write.close()
    else:
        print("initialization db")
        db_read = open("db/db.json", "r")
        read_data = db_read.read()
        db_read.close()
        return json.loads(read_data)


def db_get() -> dict:
    db_read = open("db/db.json", "r")
    read_data = db_read.read()
    db_read.close()
    return json.loads(read_data)


def dump(db: dict) -> None:
    db_file = open("db/db.json", "w")
    json.dump(db, db_file)
    db_file.close()


def room_delete(room_id: str, db: dict) -> None:
    del db[room_id]
    dump(db)
