import json
import requests
import time
import datetime


class Game:
    def __init__(self, url, session, db, seed):
        self.url = f'{url}/{seed}'
        self.session = session
        self.db = db
        self.seed = seed

    def run(self):
        self.db = self.session.get(self.url).json()
        if self.db["msg"] != "Комната занята":
            if self.db["player2"] is None:
                players = 1
            else:
                players = 2
            print(f"Вы успешно подключились к комнате {self.seed}, сейчас там находится {players} человек")
            while True:  # ожидание подключения всех
                print("Ожидание второго игрока")
                if self.db["player2"]:
                    print("Все подключились! Начинаем игру!")
                    break
                self.db = self.session.get(self.url).json()
                time.sleep(1)
            self.db = self.session.get(self.url).json()
            self.game_process()
        else:
            print(self.db['msg'])
            now = datetime.datetime.now()  # получение локального времени
            ts = time.mktime(now.timetuple())  # преобразование
            ex = self.db.get("expires")  # получение когда игра закончится
            print(f'Комната освободится через {str(datetime.datetime.fromtimestamp(ex - ts))[14:]}')

    def game_process(self):
        you = self.db.get('you')
        print(f"Вы {you}")
        while True:
            self.db = self.session.post(self.url, json={"msg": "check"}).json()
            if self.db.get('winner'):  # победитель
                if self.db.get("winner") != "Ничья":
                    print(f"Победил {self.db.get('winner')}")
                    time.sleep(3)
                else:
                    print('Ничья')
                    time.sleep(3)
                break
            if self.db.get("who") == you:
                self.get_field(self.db)
                now = datetime.datetime.now()  # получение локального времени
                ts = time.mktime(now.timetuple())  # преобразование
                ex = self.db.get("expires")  # получение когда игра закончится
                try:
                    print(
                        f'Осталось {str(datetime.datetime.fromtimestamp(ex - ts))[14:]}')  # печатаем вычтенное значение
                except:
                    print("Время на сессию истекло")
                    time.sleep(3)
                    break
                turn = input(f"Ваш ход!\n>\t")
                try:  # написать проверку и кастомные ошибки!!
                    num = int(turn)
                    if num in range(0, 10):  # да
                        print(num)
                        r = self.session.post(self.url, json={"msg": num}).json()
                        if r.get("passed"):
                            self.get_field(r)
                        else:
                            print("Что то пошло не так, повторите ввод")
                        if r.get("winner"):  # ничья
                            print(r.get("winner"))
                            time.sleep(3)
                    else:
                        print("Число должно быть от 1 до 9")
                except:
                    print("Это не число")
            else:  # ожидание хода другого игрока, если ходишь не ты
                now = datetime.datetime.now()
                ts = time.mktime(now.timetuple())
                ex = self.db.get("expires")
                try:
                    print(
                        f'Осталось {str(datetime.datetime.fromtimestamp(ex - ts))[14:]}')
                except:
                    print("Время на сессию истекло")
                    time.sleep(3)
                    break
                print("Ход другого игрока, ожидайте")
            time.sleep(1)

    def get_field(self, db):  # печать поля
        print(*db["field"][:3])
        print(*db["field"][3:6])
        print(*db["field"][6:])


class Settings:
    def __init__(self):
        self.url = input("Введите хост\n>\t")  # хост
        # локальный - http://127.0.0.1:5000"
        self.session = requests.session()
        self.seed = self.get_seed()

    def setup(self):  # настройка параметров
        try:
            self.session.get(f"{self.url}/id").json()
        except:
            print("Сервер не отвечает")
            time.sleep(3)
            exit()
        try:  # попытка загрузки куки из файла
            self.load_cookies()
        except:  # сохранение куки
            self.session.get(f"{self.url}/id").json()
            self.save_cookies()
        return self.session, self.url, self.seed

    def save_cookies(self):  # сохранение куки в фаул
        with open('cookies.json', 'w') as f:
            json.dump(requests.utils.dict_from_cookiejar(self.session.cookies), f)

    def load_cookies(self):  # загрузка куки из файла
        with open('cookies.json') as f:
            data = json.loads(f.read())
            if data:
                self.session.cookies.update(json.load(open('cookies.json')))
            else:
                self.session.get(f"{self.url}/id").json()
                self.save_cookies()

    def get_seed(self):  # ввод сида
        return input("Введите сид:\t")

    def send(self, msg):
        return self.session.post(self.url, json={"msg": msg}).json()


def greeting():  # приветствие
    print('''
    !) Правила:
    Для игры нужно два человека / клиента
    После этого сообщения у вас запросят хост, введите его по шаблону:
    http://<ip>:<port>
    Затем, введите любой сид комнаты, и если он не занят вас закинет в игру
    Иначе, повторите попытку с любым другим сидом
    После подключения двух игроков можно играть,
    Для хода напишите число свободной клетки
    Старайтесь воздержаться от вводв во время ожидания хода второго игрока
    На одну комнату с момента создания дается 5 минут
    Приятной игры!
    ''')


def main():
    greeting()
    settings = Settings()
    session, url, seed = settings.setup()
    game = Game(url=f"{url}/game", session=session, db={}, seed=seed)
    time.sleep(1)
    game.run()


if __name__ == "__main__":
    main()
