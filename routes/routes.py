import json
import random
import datetime
import time
from flask import Flask, request, send_from_directory, session, Blueprint
from db import dbf
from funcs import generate, get, post

bp = Blueprint(
    name="server",
    import_name=__name__
)
db = dbf.ini()

@bp.route("/")
def index() -> str:
    return """
    <link rel="shortcut icon" href="https://img.icons8.com/emoji/50/000000/snake-emoji.png" type="image/x-icon">
    <title>tic tac toe</title>
    <style>body {margin:0;background-image:
    url(https://cdn.discordapp.com/attachments/842420145913856030/921475660000661554/cbee52fca11707b4_1920xH.jpg);
    background-repeat: no-repeat;background-position: center;background-attachment: fixed;background-size: 2000px;}
    a:link, a:visited{color: black;} a:hover, a:active {text-decoration: underline;color: purple;}
    h1{overflow: hidden;text-align: center;font-size: 30px;font-family: "Franklin Gothic Medium";text-align: center;
    color: #000000;background-color: #FFFFFF;}</style><h1>Для игры в Крестики нолики нужно позвать друга,
    <a href="download" target="_blank">скачать клиент</a>, запустить скрипт,  и следовать инструкциям </h1>
    """


@bp.route("/download", methods=['GET', 'POST'])
def download():  # -> file
    return send_from_directory(directory="", path="client.py")


@bp.route("/egg")
def egg() -> str:
    return """
    <img src="https://cdn.discordapp.com/attachments/842420145913856030/921474842270765096/5.gif"  width="600"/>
    """


@bp.route("/id")
def player_id() -> dict:  # генератор - получатель id
    try:  # проверка session[id] из cookie is None
        is_id_none = session["id"]
    except KeyError:
        session["id"] = generate.id()
    return {"id": session["id"]}


@bp.route("/game/<string:game_seed>", methods=["GET"])  # инициализация и проверка куки ☺
def game(game_seed: str) -> dict:
    return get.game(game_seed, dbf.db_get())


@bp.route("/game/<string:game_seed>", methods=["POST"])  # это когда пользователь отправляет
def make_move(game_seed: str) -> dict:
    return post.move(game_seed, dbf.db_get())
