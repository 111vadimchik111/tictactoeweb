from flask import Flask
from funcs import preload, key_func
from routes import routes
# ни в экзамплах ни тут у меня не завелся импорт с ./.. а этот работает
app = Flask(__name__)

app.register_blueprint(routes.bp)

if __name__ == "__main__":
    app.secret_key = key_func.key()
    host, port = preload.preload()
    app.run(host=host, port=port)
