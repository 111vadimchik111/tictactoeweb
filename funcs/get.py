import datetime
import time
from flask import session
from funcs import generate
from db import dbf


def game(game_seed: str, db: dict) -> dict:
    try:  # проверка session[id] из cookie is None
        is_id_none = session["id"]
    except KeyError:
        session["id"] = generate.id()
    if db.get(game_seed):  # проверка и удаление поля по окончанию игры или таймера
        now = datetime.datetime.now()
        ts = time.mktime(now.timetuple())
        if db[game_seed].get("expires") and \
                (not (db[game_seed].get("online")) or (db[game_seed].get("expires") < ts)):
            print(f"Room {game_seed} deleted")
            dbf.room_delete(game_seed, db)
    if not db.get(game_seed):  # generate field if not exists
        db[game_seed] = {"field": [1, 2, 3, 4, 5, 6, 7, 8, 9]}
        dbf.dump(db)
    p1 = db[game_seed].get("player1", None)
    p2 = db[game_seed].get("player2", None)
    if p2 == session["id"]:  # reconnect player 2 if cookie id == id of player1
        return dict(db.get(game_seed), **{"you": "O"})
    if p1 is None:  # generate player1 if not exist
        db[game_seed].update({"player1": session["id"], "player2": p2, 'msg': 'Игрок 1 присоеденился!'})
        dbf.dump(db)
        return dict(db.get(game_seed), **{"you": "X"})
    if p1 == session["id"]:  # reconnect player 1 if cookie id == id of player1
        return dict(db.get(game_seed), **{"you": "X"})
    if p2 is None:  # generate player2 if not exist и добавление когда истекает игра
        now = datetime.datetime.now()
        ts = time.mktime(now.timetuple())
        of = 300
        expires = ts + of
        db[game_seed].update({"player1": p1, "player2": session["id"],
                              'msg': 'Игрок 2 присоеденился!', "online": True, "expires": expires})
        dbf.dump(db)
        return dict(db.get(game_seed), **{"you": "O"})  # dict то же самое что и json

    return {"msg": 'Комната занята', "expires": db[game_seed].get('expires')}  # сообщение для людей которых нет в дб
