from db import dbf
from funcs import generate
def key():
    try:
        return dbf.key_get()
    except FileNotFoundError:
        dbf.key_save(generate.key())
        return dbf.key_get()
