from flask import request, session
from funcs import turn, swap, check, generate
from db import dbf


def move(game_seed: str, db: dict) -> dict:
    X = db[game_seed].get("player1")
    O = db[game_seed].get("player2")
    try:  # проверка session[id] из cookie is None
        pid = session["id"]
    except KeyError:
        pid = session["id"] = generate.id()
    if db[game_seed].get("online"):
        if not db[game_seed].get("who"):
            db[game_seed].update({"who": "X"})
            db[game_seed].update({"turn": 0})
        if (pid == X) or (pid == O):  # к игре допускакем только пользователей которые есть в дб
            if request.get_json()["msg"] == "check":
                return db[game_seed]
            if db[game_seed]["turn"] == 8:
                db[game_seed].update({"online": False, "winner": "Ничья"})
                dbf.dump(db)
                return dict(db[game_seed], **{"passed": True})
            msg = request.get_json()
            msg = msg.get("msg")
            msg = int(msg)
            if turn.try_turn(msg, game_seed, db[game_seed].get("who"), db):
                checkis = check.win(db[game_seed].get("field"))
                db[game_seed].update({"turn": db[game_seed]["turn"] + 1})
                if checkis:
                    db[game_seed].update({"online": False, "winner": checkis})
                    dbf.dump(db)
                    return dict(db[game_seed], **{"passed": True})
                db[game_seed].update({"who": swap.swap(db[game_seed].get("who"))})
                dbf.dump(db)
                return dict(db[game_seed], **{"passed": True})
            return dict(db[game_seed], **{"passed": False})
    elif (pid == X) or (pid == O):
        return db[game_seed]
    return {"msg": 'Комната занята', "expires": db[game_seed].get('expires')}
