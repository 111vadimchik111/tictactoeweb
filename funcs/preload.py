import json
from db import dbf

def preload() -> tuple[str, int]:
    host = "127.0.0.1"
    port = 5000
    try:
        run = open("db/conf.txt", "r")
        read_data = run.read()
        conf = json.loads(read_data)
        host = conf["host"]
        port = conf["port"]
        print(f"Загружен конфиг {host}:{port}")
    except FileNotFoundError:
        print("Не удалось загрузить конфиг, воспользуйтесь командой 'save' для сохранения конфига")
    cm = "help"
    while True:
        if cm == "help":
            print(f"""
                'help' для показа этого сообщения
                'local'(127.0.0.1) или 'web'(0.0.0.0) для смены хоста и порта 
                для локальной/глобальной сети (сейчас {host}:{port})
                'run' или пустая строка для запуска
                'save' для сохранения значения host в файл
                'clear' для отчистки дб
                'exit' для завершения работы программы
                """)
        elif cm[:3] in ["run", ""]:
            print(f"Запуск")
            return host, port
        elif cm[:5] == "local":
            host = "127.0.0.1"
            port = 5000
            print(f"Смена хоста на {host}:{port}(local)")
        elif cm[:3] == "web":
            host = "0.0.0.0"
            port = 80
            print(f"Смена хоста на {host}:{port}(web)")
        elif cm[:4] == "save":
            conf_write = open("db/conf.txt", "w")
            json.dump({"host": host, "port": port}, conf_write)
            conf_write.close()
            print(f"Конфиг {host}:{port} сохранен")
        elif cm[:5] == "clear":
            dbf.dump({})
            print("Дб отчищена")
        elif cm[:4] == "exit":
            exit()
        else:
            print("Что то пошло не так!, Проверьте правильность написания команды")
        cm = input("Ввод:\n>\t")
