import random
from db import dbf


def id() -> str:
    dbf.granted()
    while True:
        chars = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ"
        others = "1234567890`~!@#$%^&*()_+-=|/.,<>"
        fin = chars + others
        out = ""
        for i in range(32):
            out += fin[random.randint(0, len(fin) - 1)]
        mass = dbf.granted_get()
        if not (out in mass):
            dbf.granted_save(out)
            return out


def key() -> str:
    chars = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ"
    others = "1234567890`~!@#$%^&*()_+-=|/.,<>"
    fin = chars + others
    out = ""
    for i in range(64):
        out += fin[random.randint(0, len(fin) - 1)]
    return out
