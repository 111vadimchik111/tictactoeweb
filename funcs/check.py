def win(field: list) -> str or False:
    for i in [[0, 1, 2], [3, 4, 5], [6, 7, 8], [0, 3, 6],
              [1, 4, 7], [2, 5, 8], [0, 4, 8], [2, 4, 6]]:
        if field[i[0]] == field[i[1]] == field[i[2]]:
            return field[i[0]]
    return False
