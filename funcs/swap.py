def swap(turn: str) -> str:
    if turn.upper() == "X":
        return "O"
    return "X"
