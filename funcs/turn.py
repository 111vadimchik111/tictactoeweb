from db import dbf


def try_turn(msg: int, seed: str, who: str, db: dict) -> bool:
    if msg in db[seed].get("field"):
        db[seed]["field"][msg - 1] = who
        dbf.dump(db)
        return True
    else:
        return False
